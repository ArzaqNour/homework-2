#!/usr/bin/env python
def main():
    print 'Welcome to AIMS module'

import math
import numpy as np

def std(deviation):
    N=len(deviation) 
    mean=0
    for x in deviation:
         mean=mean+x
    mean=float(mean)/N     
    v=0
    for i in deviation:
        v=v+(i-mean)**2
    y=np.sqrt(float(v)/N)
    return y

def avg_range(lst):
    mean=0
    N=0
    for i in lst:
        y= open(i)
        for j in y:
            e=j.strip().split(": ")
            if e[0]=='Range':
                 mean=mean+float(e[1])
                 N=N+1 
        y.close()
    return float(mean)/N
            


    
if __name__=="__main__":
    main()
