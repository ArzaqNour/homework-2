#!/usr/bin/env python
from nose.tools import assert_almost_equal
import aims
def test_positive():
    List= [1,2,3]
    abss = aims.std(List)
    exp = 0.81649658092
    assert_almost_equal(abss, exp)

def test_positive2():
    List= [10,20,30]
    abss = aims.std(List)
    exp = 8.16496580928
    assert_almost_equal(abss, exp)

def test_min3():
    List= [2.5,5,7.5]
    abss = aims.std(List)
    exp =2.04124145232
    assert_almost_equal(abss, exp)

def test_poy4():
    List= [100,200,300]
    abss = aims.std(List)
    exp =81.6496580928
    assert_almost_equal(abss, exp)

